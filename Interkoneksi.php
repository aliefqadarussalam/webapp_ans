<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Justifiable 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20130801

-->
<?php
require_once 'Fungsi.php';

$fungsi = new Fungsi();
?>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <script type="text/javascript" src="jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="jquery.slidertron-1.1.js"></script>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800|Open+Sans+Condensed:300,700" rel="stylesheet" />
        <link href="default.css" rel="stylesheet" type="text/css" media="all" />
        <link href="fonts.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="tabel.css" rel="stylesheet" type="text/css" media="all" />
        <script src="jquery.js"></script>
        <script src="MyMotion.js"></script>


        <!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
    </head>
    <body>
        <div id="logo" class="container">
            <h1><span class="icon icon-lock icon-size"></span><a href="#">ANS <span>BANK INFORMATION SYSTEM</span></a></h1>
            <!--	<p>Template Design by <a href="http://www.freecsstemplates.org"></a></p> -->
        </div>

        <div id="wrapper" class="container">
            <div id="menu" class="container">
                <ul>
                    <li><a href="index.php" accesskey="1" title="">Homepage</a></li>
                    <li class="current_page_item"><a href="interkoneksi.php" accesskey="1" title="">Interkoneksi</a></li>
                    <li><a href="user.php" accesskey="2" title="">User</a></li>
                    <li><a href="#" accesskey="3" title="">Tentang Kami</a></li>
                    <li><a href="#" accesskey="4" title="">Hubungi Kami</a></li>
                    <!-- <li><a href="#" accesskey="5" title="">Contact Us</a></li> -->
                </ul>
            </div>

            <div id="three-column" class="container">
                <!--<div><span class="arrow-down"></span></div>-->
                <div id="three-column"  class="" style="">  

                    <h2>DATA INTERKONEKSI</h2>
                    <br/>
                    <p><button type="submit" onclick="hideTabelUser('#tabel_user', 50)" class="btn btn-primary">
                            <span class="glyphicon glyphicon-new-window"></span>
                            Create</button></p>



                    <table id="tabel_user" border="2" cellpadding="10" cellspacing="0" align="center" class="table-bordered table-striped">                    
                        <tr align="center">
                            <th>Sandi Bank</th>
                            <th>Nama Bank</th>
                            <th>Alamat Bank</th>
                            <th>ip</th>
                            <th>Driver</th>
                            <th>User Database</th>
                            <th>Password Database</th>
                            <th>port</th>
                            <th>Nama Database</th>
                            <th>Status Bank APEC</th>
                            <th>No Rek ABA</th>
                            <th>No Rek ABP Deposit</th>
                            <th>operasi</th>
                        </tr>

                        <?php
                        $index = 0;
                        require_once('DB_Function.php');
                        $db = new DB_Function();

                        $select = $db->selectAllIntercon();

                        while ($result = mysql_fetch_array($select)) {
                            ?>

                            <tr align="center">
                                <td>  &nbsp  <?php echo $result[0]; ?>    	&nbsp </td>
                                <td>  &nbsp  <?php echo $result[1]; ?>    	&nbsp </td>
                                <td>  &nbsp  <?php echo $result[2]; ?>     &nbsp </td>
                                <td>  &nbsp  <?php echo $result[3]; ?>      &nbsp </td>
                                <td>  &nbsp  <?php echo $result[4]; ?>      &nbsp </td>
                                <td>  &nbsp  <?php echo $result[5]; ?>      &nbsp </td>
                                <?php
                                $db_passwordDec = $fungsi->DecryptText($result[6], "ADMIN123");
                                $db_passwordDec = $fungsi->DecryptText($db_passwordDec, "ADMIN123");
                                ?>
                                <td>  &nbsp  <?php echo $db_passwordDec; ?>      &nbsp </td>
                                <td>  &nbsp  <?php echo $result[7]; ?>      &nbsp </td> 
                                <td>  &nbsp  <?php echo $result[8]; ?>      &nbsp </td>
                                <td>  &nbsp  <?php echo $result[9]; ?>      &nbsp </td>
                                <td>  &nbsp  <?php echo $result[10]; ?>      &nbsp </td>
                                <td>  &nbsp  <?php echo $result[11]; ?>      &nbsp </td>

                                <td>
                                    <button type="button" onclick="showMe('<?php echo "#form" . $index; ?>', 'fast')" class="btn btn-mini btn-info"><span class="glyphicon glyphicon-edit"></span>
                                        Edit
                                    </button>

                                    <button type="button" onclick="deleteMeIntercon('<?php echo "#sandibank" . $index; ?>')" class="btn btn-mini btn-warning"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                </td> 
                            </tr>

                            <tr>
                                <td class="rowHide" id="<?php echo 'form' . $index; ?>" style="display:none; text-align:center" colspan="10">
                                    <form>  
                                        <table align="center">
                                            <tr>
                                                <td align="right">Sandi Bank :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'sandibank' . $index; ?>" type="text" name="username" required="required" value="<? echo $result[0]; ?>" disabled="disabled">                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Nama Bank :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'namaBank' . $index; ?>" type="text" name="password" required="required" value="<? echo $result[1]; ?>">                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Alamat Bank :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'alamatBank' . $index; ?>" type="text" name="alamatBank" required="required" value="<? echo $result[2]; ?>">                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">IP :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'ip' . $index; ?>" type="text" name="ip" required="required" value="<? echo $result[3]; ?>">                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Driver :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'driver' . $index; ?>" type="text" name="nama" required="required" value="<? echo $result[4]; ?>">                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">User Database :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'user_db' . $index; ?>" type="text" name="jabatan" required="required" value="<? echo $result[5]; ?>">                        	</td>
                                            </tr>
                                            <tr>
                                                <td align="right">Password Database :</td>
                                                <td align="left">
                                                    <?php
                                                    $db_passwordDec = $fungsi->DecryptText($result[6], "ADMIN123");
                                                    $db_passwordDec = $fungsi->DecryptText($db_passwordDec, "ADMIN123");
                                                    ?>
                                                    <input id="<?php echo 'db_password' . $index; ?>" type="text" name="db_password" required="required" value="<? echo $db_passwordDec; ?>">                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Port :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'port' . $index; ?>" type="text" name="email" required="required" value="<? echo $result[7]; ?>">                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Nama Database :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'dbName' . $index; ?>" type="text" name="status" required="required" value="<? echo $result[8]; ?>">                                
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="right">Status Bank APEC :</td>
                                                <td align="left">
                                                    <input id="<?php echo 'status_bankApec' . $index; ?>" type="text" name="level" required="required" value="<? echo $result[9]; ?>">                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">No Rek ABA :</td>
                                                <td align="left">
                                                    <p>
                                                        <input id="<?php echo 'norek_aba' . $index; ?>"type="text" name="image" required="required" value="<? echo $result[10]; ?>">                                
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">No Rek ABP Deposit  :</td>
                                                <td align="left">
                                                    <p>
                                                        <input id="<?php echo 'norek_abp_deposit' . $index; ?>"type="text" name="sandiBank" required="required" value="<? echo $result[11]; ?>">                                
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr align="right">


                                                <td>                    
                                                <!--<input type="button" value="  Batal  " onclick="hideMe('.rowHide', 'fast')" />-->
                                                    <button onclick="hideMe('.rowHide', 'fast')" class="btn btn-info"><span class="glyphicon glyphicon-remove-sign"></span> Batal</button>
                                                </td>
                                                <td align="right">   

                                                    <button value="  Simpan  " onclick="saveMeIntercon('<?php echo "#sandibank" . $index; ?>', '<?php echo "#namaBank" . $index; ?>', '<?php echo "#alamatBank" . $index; ?>', '<?php echo "#ip" . $index; ?>',  '<?php echo "#driver" . $index; ?>', '<?php echo "#user_db" . $index; ?>', '<?php echo "#db_password" . $index; ?>', '<?php echo "#port" . $index; ?>', '<?php echo "#dbName" . $index; ?>', '<?php echo "#status_bankApec" . $index; ?>', '<?php echo "#norek_aba" . $index; ?>', '<?php echo "#norek_abp_deposit" . $index; ?>', 'updateIntercon')" class="btn btn-primary"/><span class="glyphicon glyphicon-save" /></span> Simpan</button>
                                           <!--  <button value="  Simpan  " onclick="saveMe('<?php echo "#username" . $index; ?>', '<?php echo "#password" . $index; ?>', '<?php echo "#nama" . $index; ?>', '<?php echo "#jabatan" . $index; ?>', '<?php echo "#email" . $index; ?>', '<?php echo "#status" . $index; ?>', '<?php echo "#level" . $index; ?>', '<?php echo "#image" . $index; ?>', '<?php echo "#sandiBank" . $index; ?>', 'update')" class="btn btn-primary"/><span class="glyphicon glyphicon-save"></span> Simpan</button>-->
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>                     
                            <?php
                            $index++;
                        }
                        ?>  
                    </table>


                    <table id="form_input"  style="display: none" border="2" cellpadding="10" cellspacing="0" align="center" >
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>
                                        <td align="right">Sandi Ban :</td>
                                        <td align="left">
                                            <input id="<?php echo 'sandibank' . $index; ?>" type="text" name="username" required="required" value="<? echo $result[0]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Nama Bank :</td>
                                        <td align="left">
                                            <input id="<?php echo 'namaBank' . $index; ?>" type="text" name="password" required="required" value="<? echo $result[1]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Alamat Bank :</td>
                                        <td align="left">
                                            <input id="<?php echo 'alamatBank' . $index; ?>" type="text" name="alamatBank" required="required" value="<? echo $result[2]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">IP :</td>
                                        <td align="left">
                                            <input id="<?php echo 'ip' . $index; ?>" type="text" name="ip" required="required" value="<? echo $result[3]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Driver :</td>
                                        <td align="left">
                                            <input id="<?php echo 'driver' . $index; ?>" type="text" name="nama" required="required" value="<? echo $result[4]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">User Database :</td>
                                        <td align="left">
                                            <input id="<?php echo 'user_db' . $index; ?>" type="text" name="jabatan" required="required" value="<? echo $result[5]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Password Database :</td>
                                        <td align="left">
                                            <input id="<?php echo 'db_password' . $index; ?>" type="text" name="db_password" required="required" value="<? echo $result[6]; ?>">                                
                                        </td>
                                    </tr>    
                                    <tr>
                                        <td align="right">Port :</td>
                                        <td align="left">
                                            <input id="<?php echo 'port' . $index; ?>" type="text" name="email" required="required" value="<? echo $result[7]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Nama Database :</td>
                                        <td align="left">
                                            <input id="<?php echo 'dbName' . $index; ?>" type="text" name="status" required="required" value="<? echo $result[8]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Status Bank APEC :</td>
                                        <td align="left">
                                            <input id="<?php echo 'status_bankApec' . $index; ?>" type="text" name="level" required="required" value="<? echo $result[9]; ?>">                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">No Rek ABA :</td>
                                        <td align="left">
                                            <p>
                                                <input id="<?php echo 'norek_aba' . $index; ?>"type="text" name="image" required="required" value="<? echo $result[10]; ?>">                                
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">No Rek ABP Deposit :</td>
                                        <td align="left">
                                            <p>
                                                <input id="<?php echo 'norek_abp_deposit' . $index; ?>"type="text" name="sandiBank" required="required" value="<? echo $result[11]; ?>">                                
                                            </p>
                                        </td>
                                    </tr>

                                    <tr align="right">
                                        <td>     
                                            <button onclick="btn_batal()" class="btn btn-mini btn-danger"><span class="glyphicon glyphicon-remove-sign"></span> Batal</button>

                                        </td>
                                        <td align="right">  

                                            <button onclick="saveMeIntercon('<?php echo "#sandibank" . $index; ?>', '<?php echo "#namaBank" . $index; ?>', '<?php echo "#alamatBank" . $index; ?>', '<?php echo "#ip" . $index; ?>', '<?php echo "#driver" . $index; ?>', '<?php echo "#user_db" . $index; ?>', '<?php echo "#db_password" . $index; ?>', '<?php echo "#port" . $index; ?>', '<?php echo "#dbName" . $index; ?>', '<?php echo "#status_bankApec" . $index; ?>', '<?php echo "#norek_aba" . $index; ?>', '<?php echo "#norek_abp_deposit" . $index; ?>', 'createIntercon')" class="btn btn-mini btn-primary">

                                                <span class="glyphicon glyphicon-save"></span> Simpan editan</button>



<!--<input type="button" value="  Simpan  " onclick="saveMeIntercon('<?php echo "#sandibank" . $index; ?>', '<?php echo "#namaBank" . $index; ?>', '<?php echo "#alamatBank" . $index; ?>', '<?php echo "#ip" . $index; ?>', '<?php echo "#driver" . $index; ?>', '<?php echo "#user_db" . $index; ?>', '<?php echo "#db_password" . $index; ?>', '<?php echo "#port" . $index; ?>', '<?php echo "#dbName" . $index; ?>', '<?php echo "#status_bankApec" . $index; ?>', '<?php echo "#norek_aba" . $index; ?>', '<?php echo "#norek_abp_deposit" . $index; ?>', 'createIntercon')" />-->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>                    

                </div>

                <div id="page" style="margin-top:30px">
                    <div>
                        <span class="arrow-down"></span>
                    </div>

                    <div id="box1">
                        <div class="title">
                            <h2>Selamat datang di website kami</h2>
                            <span class="byline">Integer sit amet pede vel arcu aliquet pretium</span>
                        </div>
                        <p>This is <strong>Justifiable</strong>, a free, fully standards-compliant CSS template designed by <a href="http://www.freecsstemplates.org/" rel="nofollow">FreeCSSTemplates.org</a>. The photos in this template are from <a href="http://fotogrph.com/"> Fotogrph</a>. This free template is released under a <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attributions 3.0</a> license, so you are pretty much free to do whatever you want with it (even use it commercially) provided you keep the links in the footer intact. Aside from that, have fun with it :) </p>
                        <a href="#" class="button">Learn More</a>
                    </div>

                    <div id="box2">
                        <div class="title">
                            <h2>Tentang Kami</h2>
                            <span class="byline">Artha Nusa Sembada</span> 
                        </div>
                        <p>     Lembaga Keuangan Mikro (Micro Finance Institution) sebagai ujung tombak dalam pengentasan kemiskinan dan dalam mendorong ekonomi masyarakat telah mendapat pengakuan secara internasional. Peranan Lembaga Keuangan Mikro (Micro Finance Institution) dalam memberikan akses permodalan kepada pelaku Usaha Mikro dan Menengah merupakan salah satu jalan keluar dalam pengentasan kemiskinan.
                            <br></br>
                            Bentuk Lembaga Keuangan Mikro (Micro Finance Institution) formal seperti Bank Perkreditan Rakyat (BPR) dan Perusahaan Modal Ventura merupakan salah satu inovasi yang paling berhasil dalam pembangunan sosial-ekonomi serta memiliki kontribusi yang penting dalam pencapaian Sasaran Pembangunan Milenium (Millenium Development Goals).

                            <br></br>
                            Hal utama yang menjadi kunci keberhasilan dalam memberikan pelayanan tersebut adalah lokasi yang dekat dengan masyarakat yang membutuhkan, prosedur pelayanan yang sederhana dan lebih mengutamakan pendekatan personal serta fleksibilitas pola dan model pinjaman.</p>	
                        <a href="#" class="button">Learn More</a>
                    </div>                
                </div>
                <!--<div>
                    <span class="arrow-down"></span>
                </div>-->

            </div>
            <div id="copyright">
                <p>Copyright (c) 2013 Sitename.com. All rights reserved. | Photos by <a href="http://fotogrph.com/">Fotogrph</a> | Design by <a href="http://www.freecsstemplates.org/" rel="nofollow">FreeCSSTemplates.org</a>.</p>
            </div>
        </div>
    </body>
</html>
