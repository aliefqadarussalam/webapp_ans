<?php
     class Proses {
         
         private $jsonrpc;
         public $method;
         private $id;
         private $result = array();
         public $strKey;
         public $sekarang;
         public $tglSystemArr;
         public $tglSytem;
         public $fungsi;
         private $db;
         public $no_urut;
         public $jam;

         function __construct() {
             $this->jsonrpc = "2.0";
             $this->id = "id Disini";
             $this->strKey = "ADMIN123";
             $this->sekarang = date('Y-m-d h:i:s');
             require_once 'DB_Functions.php';
             require_once 'Fungsi.php';
             $this->db = new DB_Functions();
             $this->db->connect();
             $this->db->select('settanggal', 'settanggal');
             $tglSystemArr = $this->db->getResult();
             $this->tglSytem = $tglSystemArr["settanggal"];
             $this->fungsi = new Fungsi();
             $this->jam = date("His");
             $tanggal = new DateTime($this->tglSytem);
             $this->no_urut = date_format($tanggal, "ymd");
             $this->no_urut = $this->no_urut.$this->jam;             
         }
                  
         public function inquiryAngsuranTerjadwal($id_ao, $id_request = null, $npk, $id_bpr, $method_name) {
             
             $db = new DB_Functions();
             
             $fungsi = new Fungsi();
             $db->connect();
             $method_name = "fastbank.inquiry.angsuran.terjadwal";
             $tgl = $this->tglSytem;
             $tables = "rekredit, datapokok, angsuran";
             /*$query = "rekredit.norek AS npk, datapokok.nama, if(angsuran.angsuran = 0, 1, angsuran.angsuran) AS angsuran_ke, 
                       if(rekredit.tpokok > 0, rekredit.tpokok, rekredit.pokok) AS angsuran_pokok, if(rekredit.tbunga > 0, rekredit.tbunga, rekredit.bunga) AS angsuran_bunga, 
                       rekredit.denda AS angsuran_denda, rekredit.saldo AS saldo_pokok, (rekredit.jmlbunga - rekredit.bngdibayar) AS saldo_bunga, rekredit.coll as kolektibilitas, 
                       rekredit.tpokok as tunggak_pokok, rekredit.tbunga as tunggak_bunga"; */
             /*$query = "rekredit.norek AS npk, datapokok.nama, if(angsuran.angsuran = 0, 1, angsuran.angsuran) AS angsuran_ke, 
                       if($tgl >= angsuran.tgl , rekredit.tpokok, rekredit.pokok) AS angsuran_pokok, if($tgl >= angsuran.tgl, rekredit.tbunga, rekredit.bunga) AS angsuran_bunga, 
                       rekredit.denda AS angsuran_denda, rekredit.saldo AS saldo_pokok, (rekredit.jmlbunga - rekredit.bngdibayar) AS saldo_bunga, rekredit.coll as kolektibilitas, 
                       rekredit.tpokok as tunggak_pokok, rekredit.tbunga as tunggak_bunga";
              */
		  $query = "rekredit.norek AS npk, datapokok.nama, if(angsuran.angsuran = 0, 1, angsuran.angsuran) AS angsuran_ke, 
                       rekredit.pokok AS angsuran_pokok, rekredit.bunga AS angsuran_bunga, 
                       rekredit.denda AS angsuran_denda, rekredit.saldo AS saldo_pokok, (rekredit.jmlbunga - rekredit.bngdibayar) AS saldo_bunga, rekredit.coll as kolektibilitas, 
                       rekredit.tpokok as tunggak_pokok, rekredit.tbunga as tunggak_bunga, angsuran.tgl as tgl_angsuran";
                       
            $where = "datapokok.idnama = rekredit.idnama AND angsuran.norek = rekredit.norek AND rekredit.norek = '$npk' AND MONTH(angsuran.`Tgl`) = MONTH('$tgl') AND YEAR(angsuran.tgl) = YEAR('$tgl')";

//	    $db = new DB_Functions();
            $db->select($tables, $query, $where, null);
             
             $result = $db->getResult();
        
             if(count($result) != 0) {
                 $fungsi = new Fungsi();
		     $fungsi->array_unshift_assoc($result, "id_bpr", $id_bpr);
                 $fungsi->array_unshift_assoc($result, "method_name", $method_name);
                 if($result["kolektibilitas"] == "1") {
                     $result["kolektibilitas"] = "Lancar";
                 } elseif ($result["kolektibilitas"] == "2") {
                     $result["kolektibilitas"] = "Kurang Lancar";
                 } elseif ($result["kolektibilitas"] == "3") {
                     $result["kolektibilitas"] = "Diragukan";
                 } else {
                     $result["kolektibilitas"] = "Macet";
                 }
		     
		     if(strtotime($tgl) >= strtotime($result["tgl_angsuran"])){
		         if($result["tunggak_pokok"] == 0) {
		             $result["angsuran_pokok"] = $result["angsuran_pokok"];
                     
		         } else {
		             $result["angsuran_pokok"] = $result["tunggak_pokok"];
		         }
                 
                 if($result["tunggak_bunga"] == 0) {
                     $result["angsuran_bunga"] = $result["angsuran_bunga"];
                     
                 } else {
                     $result["angsuran_bunga"] = $result["tunggak_bunga"];
                 }
				        //$result["angsuran_pokok"] = $result["tunggak_pokok"];
                 		//$result["angsuran_bunga"] = $result["tunggak_bunga"];
             }else{
				$result["angsuran_pokok"] = $result["angsuran_pokok"];
                $result["angsuran_bunga"] = $result["angsuran_bunga"];
		     }	
                 $result["saldo_pokok"] = $fungsi->DecryptText($fungsi->DecryptText($result["saldo_pokok"], $this->strKey), $this->strKey);
                 $result["saldo_pokok"] = $result["saldo_pokok"];
                 $result["saldo_bunga"] = $result["saldo_bunga"];
                 $result["angsuran_denda"] = $result["angsuran_denda"];
                 $result["tunggak_pokok"] = $result["tunggak_pokok"];
                 $result["tunggak_bunga"] = $result["tunggak_bunga"];
                 $result["tanggal_system"] = $tgl;
                 $response = array("jsonrpc"=>$this->jsonrpc, "result"=>$result, "id"=>$id_request);
             } else {
                 $response = $this->errorResponse($id_request, "01");
             }
             
             return $response;

         }
         
         public function inquiryAngsuranTerbuka($id_ao, $id_request = null, $npk, $id_bpr, $method_name) {
            
            $db = new DB_Functions();
            
             $db->connect();
             
             $db->select("rekredit, datapokok, angsuran","rekredit.norek AS npk, datapokok.nama, if(angsuran.angsuran = 0, 1, angsuran.angsuran) AS angsuran_ke, rekredit.saldo AS saldo_pokok, (rekredit.jmlbunga - rekredit.bngdibayar) AS saldo_bunga, rekredit.denda AS saldo_denda", 
                           "datapokok.idnama = rekredit.idnama AND angsuran.norek = rekredit.norek AND rekredit.norek = '$npk' AND MONTH(angsuran.`Tgl`) = MONTH('$this->tglSytem') AND YEAR(angsuran.tgl) = YEAR('$this->tglSytem')", null);
             
             $result = $db->getResult();
             
             if(count($result) != 0) {
                 $fungsi = new Fungsi();
                 
                 $fungsi->array_unshift_assoc($result, "id_bpr", $id_bpr);
                 $fungsi->array_unshift_assoc($result, "method_name", $method_name);
                 $result["saldo_pokok"] = $fungsi->DecryptText($fungsi->DecryptText($result["saldo_pokok"], $this->strKey), $this->strKey);
                 $result["saldo_bunga"] = $result["saldo_bunga"];
                 $result["saldo_denda"] = $result["saldo_denda"];
                 $response = array("jsonrpc"=>$this->jsonrpc, "result"=>$result, "id"=>$id_request);
             } else {
                 $response = $this->requestNotFound($id_bpr);
             }
             
             return $response;

         }
         
         public function paymentAngsuranTerjadwal($id_ao, $id_bpr, $norek, $nominal, $method_name, $id_request) {
             
             
                 $db = new DB_Functions();
                 $db->connect();
                 
                 $query = 'rekredit.rektab as rektab, if(angsuran.angsuran = 0, 1, angsuran.angsuran) AS angsuran_ke';
                 
                 $db->select('rekredit, angsuran', $query, "rekredit.norek = angsuran.norek AND rekredit.norek = '$norek' and MONTH(angsuran.`tgl`) = MONTH('$this->tglSytem') AND YEAR(angsuran.`tgl`) = YEAR('$this->tglSytem')");
                 $norekTab = $db->getResult();
                 
                 
                 //$no_urut = date("YmdHis", $this->tglSytem);
                 
                 if(count($norekTab) != 0 ) {
                    $angsuran_ke = $norekTab["angsuran_ke"];
                    $no_rekening = $norekTab["rektab"];
                    
                    $db = new DB_Functions();
                    $db->select('rektab, datapokok, officer', 'rektab.norek, datapokok.nama, rektab.saldo, officer.namaao', "datapokok.idnama = rektab.idnama AND officer.kodeao = '$id_ao' AND rektab.norek='$no_rekening'");
                    $hasil = $db->getResult();
                 
      
                 if(count($hasil) != 0) {
                     
                     
                     $fungsi = new Fungsi();
                     $saldo_awal = $fungsi->DecryptText($fungsi->DecryptText($hasil["saldo"], $this->strKey), $this->strKey);
                     
                     $nama_nasabah = $hasil["nama"];
                     $nama_ao = $hasil["namaao"];
                     $saldo_akhir = $saldo_awal + $nominal;
                     $saldo_encrypt = $fungsi->EncryptText($fungsi->EncryptText($saldo_akhir, $this->strKey), $this->strKey);
                     
                     $db->select('mutasi', 'max(nobukti) as nobukti, operator', "operator = '$nama_ao' AND tglmut = '$this->tglSytem'");
                     
                     $nobukti = $db->getResult();
                     
                     $nobukti_akhir = $nobukti["nobukti"] + 1;
                     
                     $ins = $db->insert('mutasi',array($nobukti_akhir, $no_rekening, $nama_nasabah, $nominal, substr($this->tglSytem, 0, 10), 1, "Setor Tunai: $no_rekening", $saldo_akhir, $nama_ao, $saldo_awal, $this->no_urut, $no_rekening), 'nobukti, norek, nama, Kredit, tglmut,sandimut, ket, saldo, operator, kemaren, NOURUT, NOBUNGA');
                     if($ins) {
                         
                         $upd = $db->update('rektab', array("saldo"=>$saldo_encrypt), array("norek='$no_rekening'"));
                         if($upd) {
                             $saldo_akhir = $saldo_akhir;
                             $result = array("method_name"=>$method_name, 
                                        "id_bpr"=>$id_bpr, "npk"=>$norek, "nama"=>$nama_nasabah, 
                                        "waktu_terbayar"=>$this->sekarang, "angsuran_terbayar_ke"=>$angsuran_ke,"nominal_terbayar"=>$nominal,"id_trx_payment"=>$this->no_urut);
                            $response = array("jsonrpc"=>$this->jsonrpc, "result"=>$result, "id"=>$id_request);
                         } else {
                             $response = $this->errorResponse($id_request, "02");
                         }
                     } else {
                         $response = $this->errorResponse($id_request, "02");
                     }
    
                 } else {
                     $response = $this->errorResponse($id_request, "01");
                 }
                
                 } else {
                     $response = $this->errorResponse($id_request, '01');
                 }
                 
                 return $response;
         }
         
         public function inquirySetoranTunai($id_ao, $id_bpr, $no_rekening, $id_request, $method_name) {
             
             $db = new DB_Functions();
             
             
             $db->connect();
             $db->select('datapokok, rektab', 'rektab.norek as no_rekening, datapokok.nama, rektab.saldo, rektab.salmin as saldo_minimal', "rektab.norek='$no_rekening' AND rektab.idnama = datapokok.idnama");
             $result = $db->getResult();
             
             if(count($result) != 0) {
                 $fungsi = new Fungsi();
		     $fungsi->array_unshift_assoc($result, "id_bpr", $id_bpr);           
                 $fungsi->array_unshift_assoc($result, "method_name", $method_name);
                 $saldo = $fungsi->DecryptText($fungsi->DecryptText($result["saldo"], $this->strKey), $this->strKey);
                 
                 $saldo_skrg = $saldo;
                 $saldo_skrg = $saldo;
                 $result["saldo"] = $saldo_skrg;
                 $saldo_minimal = $result["saldo_minimal"];
                 $result["saldo_tersedia"] = $saldo - $result["saldo_minimal"];
                 $result["saldo_minimal"] = $result["saldo_minimal"];
                 $result["saldo_tersedia"] = $result["saldo_tersedia"];
                 $result["saldo_boleh_ditarik"] = $result["saldo_tersedia"];
                 $response = array("jsonrpc"=>$this->jsonrpc, "result"=>$result, "id"=>$id_request);
             } else {
                 $response = $this->errorResponse($id_request, "01");
             } 
             return $response;
         }
         
         public function paymentSetoranTunai($id_ao, $id_bpr, $no_rekening, $nominal, $method_name, $id_request) {
             
             $db = new DB_Functions();
             $db->connect();
             $db->select('rektab, datapokok, officer', 'rektab.norek, datapokok.nama, rektab.saldo, officer.namaao', "datapokok.idnama = rektab.idnama AND officer.kodeao = '$id_ao' AND rektab.norek='$no_rekening'");
             $hasil = $db->getResult();
             
             //$no_urut = date("YmdHis", $this->tglSytem);
             
             
             if(count($hasil) != 0) {
                 
                 
                 $fungsi = new Fungsi();
                 $saldo_awal = $fungsi->DecryptText($fungsi->DecryptText($hasil["saldo"], $this->strKey), $this->strKey);
                 
                 $nama_nasabah = $hasil["nama"];
                 $nama_ao = $hasil["namaao"];
                 $saldo_akhir = $saldo_awal + $nominal;
                 $saldo_encrypt = $fungsi->EncryptText($fungsi->EncryptText($saldo_akhir, $this->strKey), $this->strKey);
                 
                 $db->select('mutasi', 'max(nobukti) as nobukti, operator', "operator = '$nama_ao' AND tglmut = '$this->tglSytem'");
                 
                 $nobukti = $db->getResult();
                 
                 $nobukti_akhir = $nobukti["nobukti"] + 1;
                 
                 $ins = $db->insert('mutasi',array($nobukti_akhir, $no_rekening, $nama_nasabah, $nominal, substr($this->tglSytem, 0, 10), 1, "Setor Tunai: $no_rekening", $saldo_akhir, $nama_ao, $saldo_awal, $this->no_urut, $no_rekening), 'nobukti, norek, nama, Kredit, tglmut,sandimut, ket, saldo, operator, kemaren, NOURUT, NOBUNGA');
                 if($ins) {
                     
                     $upd = $db->update('rektab', array("saldo"=>$saldo_encrypt), array("norek='$no_rekening'"));
                     if($upd) {
                         $saldo_akhir = $saldo_akhir;
                         $result = array("method_name"=>$method_name, 
                                    "id_bpr"=>$id_bpr, "no_rekening"=>$no_rekening, "nama"=>$nama_nasabah, 
                                    "waktu_terbayar"=>$this->sekarang, "nominal_terbayar"=>$nominal, "saldo"=>$saldo_akhir, "id_trx_payment"=>$this->no_urut);
                        $response = array("jsonrpc"=>$this->jsonrpc, "result"=>$result, "id"=>$id_request);
                     } else {
                         $response = $this->errorResponse($id_request, "02");
                     }
                 } else {
                     $response = $this->errorResponse($id_request, "02");
                 }

             } else {
                 $response = $this->errorResponse($id_request, "01");
             }
            return $response;
             
        }
         
         public function inquiryTarikanTunai($id_ao, $id_bpr, $no_rekening, $id_request, $method_name) {
             $db = new DB_Functions();
             
             
             $db->connect();
             $db->select('datapokok, rektab', 'rektab.norek as no_rekening, datapokok.nama, rektab.saldo, rektab.salmin as saldo_minimal', "rektab.norek='$no_rekening' AND rektab.idnama = datapokok.idnama");
             $result = $db->getResult();
             
             if(count($result) != 0) {
                 $fungsi = new Fungsi();
		     $fungsi->array_unshift_assoc($result, "id_bpr", $id_bpr);           
                 $fungsi->array_unshift_assoc($result, "method_name", $method_name);
                 $saldo = $fungsi->DecryptText($fungsi->DecryptText($result["saldo"], $this->strKey), $this->strKey);
                 
                 $saldo_skrg = $saldo;
                 $result["saldo"] = $saldo_skrg;
                 $saldo_minimal = $result["saldo_minimal"];
                 $result["saldo_tersedia"] = $saldo - $result["saldo_minimal"];
                 $result["saldo_minimal"] = $result["saldo_minimal"];
                 $result["saldo_boleh_ditarik"] = $result["saldo_tersedia"];
                 //$result["saldo_tersedia"] = $result["saldo_tersedia"];
                 $response = array("jsonrpc"=>$this->jsonrpc, "result"=>$result, "id"=>$id_request);
             } else {
                 $response = $this->errorResponse($id_request, "01");
             } 
             return $response;
         }
         
         public function paymentTarikanTunai($id_ao, $id_bpr, $no_rekening, $nominal, $method_name, $id_request) {
             $db = new DB_Functions();
             $db->connect();
             $db->select('rektab, datapokok, officer', 'rektab.norek, datapokok.nama, rektab.saldo, officer.namaao, rektab.salmin', "datapokok.idnama = rektab.idnama AND officer.kodeao = '$id_ao' AND rektab.norek='$no_rekening'");
             $hasil = $db->getResult();
             
             if(count($hasil) != 0) {
                 
                 
                 $fungsi = new Fungsi();
                 $saldo_awal = $fungsi->DecryptText($fungsi->DecryptText($hasil["saldo"], $this->strKey), $this->strKey);
                 $saldo_debet = $saldo_awal-$hasil["salmin"];
                 if($saldo_debet >= $nominal ) {
                     
                 $nama_nasabah = $hasil["nama"];
                 $nama_ao = $hasil["namaao"];
                 $saldo_akhir = $saldo_awal - $nominal;
                 $saldo_encrypt = $fungsi->EncryptText($fungsi->EncryptText($saldo_akhir, $this->strKey), $this->strKey);
                 
			     $db->select('mutasi', 'max(nobukti) as nobukti, operator', "operator = '$nama_ao' AND tglmut = '$this->tglSytem'");
                 
                 $nobukti = $db->getResult();                 
                 $nobukti_akhir = $nobukti["nobukti"] + 1;
                 //$no_urut = date("YmdHis", $this->tglSytem);
                 $ins = $db->insert('mutasi',array($nobukti_akhir, $no_rekening, $nama_nasabah, $nominal, substr($this->tglSytem, 0, 10), 2, "Tarik Tunai: $no_rekening", $saldo_akhir, $nama_ao, $saldo_awal, $this->no_urut, $no_rekening), 'nobukti, norek, nama, DEBET, tglmut,sandimut, ket, saldo, operator, kemaren, nourut, NOBUNGA');
                 if($ins) {
                     
                     $upd = $db->update('rektab', array("saldo"=>$saldo_encrypt), array("norek='$no_rekening'"));
                     if($upd) {
                         $saldo_akhir = $saldo_akhir;
                         $result = array("method_name"=>$method_name, 
                                    "id_bpr"=>$id_bpr, "no_rekening"=>$no_rekening, "nama"=>$nama_nasabah, 
                                    "waktu_terbayar"=>$this->sekarang, "nominal_terbayar"=>$nominal, "saldo"=>$saldo_akhir, "id_trx_payment"=>$this->no_urut);
                        $response = array("jsonrpc"=>$this->jsonrpc, "result"=>$result, "id"=>$id_request);
                     } else {
                         $response = $this->errorResponse($id_request, "02");
                     }
                 } else {
                     $response = $this->errorResponse($id_request, "02");
                 }
                 } else {
                     
                     $response = $this->errorResponse($id_request, "03");
                 }
             } else {
                 $response = $this->errorResponse($id_request, "01");
             }
            return $response;
         }
         
        
        public function reversal($id_ao, $id_bpr, $id_trx_payment, $nominal, $id_request) {
            $db = new DB_Functions();
            $fungsi = new Fungsi();
            $db->connect();
            
            $db->select('mutasi', '*', "nourut = '$id_trx_payment'");
            $cek_mutasi = $db->getResult();
            $norek = $cek_mutasi["NOREK"];
            
            $nama_ao = $cek_mutasi["OPERATOR"];
            $nama = $cek_mutasi["NAMA"];
            $tgl_mut = $cek_mutasi["TGLMUT"];
            
            $no_urut = date("ymdHis");
            
            $db->select('rektab', '*', "norek='$norek'");
            $res = $db->getResult();
            $saldo_awal = $fungsi->DecryptText($fungsi->DecryptText($res["SALDO"], $this->strKey), $this->strKey);
            
            $db->select('mutasi', 'max(nobukti) as nobukti, operator', "operator = '$nama_ao' AND tglmut = '$this->tglSytem'");
            $nobukti = $db->getResult();
            $nobukti_akhir = $nobukti["nobukti"] + 1;
            
            if(count($cek_mutasi) != 0) {
            
                if($cek_mutasi['KREDIT'] == 0) {
                //echo "JURNAL KREDIT";
                $nominal = $cek_mutasi["DEBET"];
                $saldo_akhir = $saldo_awal + $nominal;
                
                $saldo_encrypt = $fungsi->EncryptText($fungsi->EncryptText($saldo_akhir, $this->strKey), $this->strKey);
                $insert = $db->insert('mutasi', array($norek, $nobukti_akhir,$nama,$nominal, 1, $tgl_mut, "koreksi : $norek", $saldo_akhir, $no_urut, $nama_ao, $saldo_awal, $norek), 'norek,nobukti,nama,kredit,sandimut,tglmut, ket,saldo, nourut, operator, kemaren, nobunga');
                if($insert) {
                    $update = $db->update("rektab", array("saldo"=>$saldo_encrypt), array("norek='$norek'"));
                    if($update) {
                        $response = array("jsonrpc"=>"2.0", "result"=>array("method_name"=>"fastbank.reversal", "id_bpr"=>$id_bpr, "id_trx_payment"=>$no_urut, "nominal"=>$nominal), "id"=>$id_request);
                    } else {
                        $response = $this->errorResponse($id_request, "02");
                    }
                } else {
                    $response = $this->errorResponse($id_request, "02");
                }
                
            } else {
                //echo "JURNAL DEBET";
                $nominal = $cek_mutasi["KREDIT"];
                $saldo_akhir = $saldo_awal - $nominal;
                
                $saldo_encrypt = $fungsi->EncryptText($fungsi->EncryptText($saldo_akhir, $this->strKey), $this->strKey);
                //echo "saldoencrypt".$saldo_encrypt;
                $insert = $db->insert('mutasi', array($norek, $nobukti_akhir, $nama, $nominal, 2, $tgl_mut, "koreksi : $norek", $saldo_akhir, $no_urut, $nama_ao, $saldo_awal, $norek), 'norek, nobukti, nama, debet, sandimut, tglmut, ket, saldo, nourut, operator, kemaren, nobunga');
                if($insert) {
            
                $update = $db->update("rektab", array("saldo"=>$saldo_encrypt), array("norek='$norek'"));
                if($update) {
                    $response = array("jsonrpc"=>"2.0", "result"=>array("method_name"=>"fastbank.reversal", "id_bpr"=>$id_bpr, "id_trx_payment"=>$no_urut, "nominal"=>$nominal), "id"=>$id_request);
                } else {
                    $response = $this->errorResponse($id_request, "02");
                }
            
            } else {
                $response = $this->errorResponse($id_request, "02");
                }
            }
            
            } else {
                $response = $this->errorResponse($id_request, '01');
            }
            
            return $response;
        }
        
        public function errorResponse($id_request, $error_code) {
            
            switch($error_code) {
                case "01":
                    $error = array("message"=>"Data tidak ditemukan");
                    break;
                case "02":
                    $error = array("message"=>"Data tidak benar sehingga gagal update");
                    break;
                case "03":
                    $error = array("message"=>"Saldo tidak cukup");
                    break;
            }
            $this->fungsi->array_unshift_assoc($error, "code", $error_code);
            $result = array("jsonrpc=>"=>$this->jsonrpc, "error"=>$error, "id"=>$id_request);
            return $result;
        }

	 public function inquirycobacoba($id_ao, $id_bpr, $no_rekening, $id_request, $method_name) {
             $db = new DB_Functions();
             
             
             $db->connect();
             $db->select('datapokok,rekdep,bngdep', 'rekdep.norek as no_rekening, datapokok.nama, rekdep.saldo, bngdep.pro1 as rate', "rekdep.norek='$no_rekening' AND rekdep.idnama = datapokok.idnama AND bngdep.kodebng = rekdep.kodebng");
             $result = $db->getResult();
             
             if(count($result) != 0) {
                 $fungsi = new Fungsi();
                 $fungsi->array_unshift_assoc($result, "method_name", $method_name);
                 $saldo = $fungsi->DecryptText($fungsi->DecryptText($result["saldo"], $this->strKey), $this->strKey);
                 
                 $saldo_skrg = $saldo;
                 $result["saldo"] = $saldo_skrg;
		     $bunga = $saldo * ($result["rate"]/100) / 12;		     
		     $result["bunga_bulan_ini"] = $bunga;		
		     unset($result["rate"]);
                 $response = array("jsonrpc"=>$this->jsonrpc, "result"=>$result, "id"=>$id_request);

             } else {
                 $response = $this->errorResponse($id_request, "01");
             } 
             return $response;
         }

         public function inquiryKelompok($id_ao, $id_bpr, $no_kelompok, $id_request, $method_name) {

            $db = new DB_Functions();

            $db->connect();
            $db->select("qr_angsuran p, (SELECT @nmr:=0) r", "@nmr:=@nmr+1 `no_urut`, p.norektab AS no_rekening,  p.nama AS `nama_nasabah`, p.Bpokok + p.Bbunga + p.tabungan AS `nominal_kewajiban`", "kd_kumpulan = '$no_kelompok'");
            $nasabah = $db->getResult();

            if(count($nasabah) != 0) {
                $result = array("method_name" => $method_name, "id_bpr" => $id_bpr, "no_kelompok" => $no_kelompok, "anggota_kelompok" => $nasabah);

                $response = array("jsonrpc" => $this->jsonrpc, "result" => $result, "id" => $id_request);
            } else {
                $response = $this->errorResponse($id_request, '01');
            }
            
            return $response;

         }
         
        
     }
?>