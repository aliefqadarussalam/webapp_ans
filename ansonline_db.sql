-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 06, 2013 at 08:12 AM
-- Server version: 5.1.28
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ansonline_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_intercon`
--

CREATE TABLE IF NOT EXISTS `t_intercon` (
  `sandibank` varchar(15) NOT NULL DEFAULT '-',
  `namaBank` varchar(50) NOT NULL DEFAULT '-',
  `alamatBank` varchar(50) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `driver` varchar(50) NOT NULL,
  `user_db` varchar(50) NOT NULL DEFAULT '-',
  `db_password` varchar(50) NOT NULL,
  `port` varchar(50) NOT NULL DEFAULT '-',
  `dbName` varchar(50) NOT NULL DEFAULT '-',
  `status_bankApec` int(1) NOT NULL DEFAULT '0',
  `norek_aba` varchar(50) NOT NULL DEFAULT '-',
  `norek_abp_deposit` varchar(50) NOT NULL DEFAULT '-',
  PRIMARY KEY (`sandibank`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_intercon`
--

INSERT INTO `t_intercon` (`sandibank`, `namaBank`, `alamatBank`, `ip`, `driver`, `user_db`, `db_password`, `port`, `dbName`, `status_bankApec`, `norek_aba`, `norek_abp_deposit`) VALUES
('13', '131', 'alamat1', 'localhost', 'MySQL ODBC 3.51 Driver', 'root', '', '3306', 'bpr', 1, '1', '1'),
('b', 'Nama Bank 1', 'alamat2', '127.0.0.1', 'MySQL ODBC 3.51 Driver', 'myDatabase', '', 'b', 'b', 0, 'b', 'b'),
('sandi', 'nama', 'alamat3', '127.0.0.1', 'MySQL ODBC 3.51 Driver', 'driver', '', 'port', 'namadb', 1, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `username` varchar(30) NOT NULL,
  `password` varchar(35) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `level` varchar(30) NOT NULL DEFAULT 'user',
  `img_user` text NOT NULL,
  `sandibank` varchar(15) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`username`, `password`, `nama`, `jabatan`, `email`, `status`, `level`, `img_user`, `sandibank`) VALUES
('0', '0', '0', '0', '0', 0, '0', '0', '0'),
('12223', '1', '1', '1', '1', 1, '1', '1', '1'),
('13', '1', '1', '1', '1', 1, '1', '1', '1'),
('1ruchdi3', '1', '1', '1', '1', 1, '1', '1', '1'),
('989', '8989', '889', '8989', '8989', 899, '989', '8989', '898'),
('afd', 'dfa', 'dfa', 'dsa', 'dfa', 1, 'user13', 'dsa', 'jkkjk'),
('dell', 'inspiront', 'jlk', 'jl', 'hhf', 1, 'dfsa', 'fdsa', 'fds'),
('hjf', 'g', 'jkl', 'jl', 'jkl', 1, 'user', 'jl', 'jl'),
('uio', 'jkl', 'n,', 'jlk', 'uoi', 2, 'user', 'hjk', 'hjk');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
