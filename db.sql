/*
SQLyog Ultimate v9.50 
MySQL - 5.1.28-rc-community : Database - db_tabungan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_tabungan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_tabungan`;

/*Table structure for table `rektab` */

DROP TABLE IF EXISTS `rektab`;

CREATE TABLE `rektab` (
  `NOREK` varchar(9) NOT NULL,
  `IDNAMA` varchar(6) DEFAULT NULL,
  `SALDO` varchar(50) DEFAULT NULL,
  `KODESLD` varchar(1) DEFAULT NULL,
  `TGLMASUK` date DEFAULT NULL,
  `SALMIN` double DEFAULT NULL,
  `KODEBNG` varchar(3) DEFAULT NULL,
  `TGLBNG` varchar(2) DEFAULT NULL,
  `KENAPPH` varchar(1) NOT NULL DEFAULT 'B',
  `GOLDEB` varchar(3) DEFAULT NULL,
  `KODEAO` varchar(3) DEFAULT NULL,
  `REKBNG` varchar(9) DEFAULT NULL,
  `TGLMUT` date DEFAULT '0000-00-00',
  `TERENDAH` double NOT NULL DEFAULT '0',
  `RATA2` double NOT NULL DEFAULT '0',
  `PEDBNG` double NOT NULL DEFAULT '0',
  `PEDPPH` double NOT NULL DEFAULT '0',
  `BNGAKHIR` double NOT NULL DEFAULT '0',
  `PRO` double NOT NULL DEFAULT '0',
  `HASIL` double NOT NULL DEFAULT '0',
  `RENDAH` double NOT NULL DEFAULT '0',
  `RATA` double NOT NULL DEFAULT '0',
  `JUMCETAK` double NOT NULL DEFAULT '0',
  `BRS` double NOT NULL DEFAULT '0',
  `KEL` double NOT NULL DEFAULT '0',
  `KEMAREN` double NOT NULL DEFAULT '0',
  `TUTUP` int(4) NOT NULL DEFAULT '0',
  `NOR` varchar(50) DEFAULT NULL,
  `MUTASIAKHIR` date DEFAULT '0000-00-00',
  `KET` varchar(15) NOT NULL DEFAULT 'Aktif',
  `SUFF` varchar(9) DEFAULT NULL,
  `AKTIF` int(4) DEFAULT '0',
  `SALDOENC` double DEFAULT '0',
  `TGL_ADM` date DEFAULT '0000-00-00',
  `norek_kkm` varchar(12) DEFAULT NULL,
  `tabsuk` double NOT NULL DEFAULT '0',
  `tabwajib` double NOT NULL DEFAULT '0',
  `setpokok` double NOT NULL DEFAULT '0',
  `setbunga` double NOT NULL DEFAULT '0',
  `bngsmt` double NOT NULL DEFAULT '0',
  `norek_ref` varchar(10) DEFAULT '0',
  `TGLJT` date DEFAULT NULL,
  `keterkaitan` int(1) unsigned NOT NULL DEFAULT '2',
  `pin` varchar(20) DEFAULT '1234',
  PRIMARY KEY (`NOREK`),
  KEY `IDNAMA` (`IDNAMA`) USING BTREE,
  KEY `KODEBNG` (`KODEBNG`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rektab` */

/*Table structure for table `t_intercon` */

DROP TABLE IF EXISTS `t_intercon`;

CREATE TABLE `t_intercon` (
  `sandibank` varchar(15) NOT NULL DEFAULT '-',
  `nama_bank` varchar(50) DEFAULT '-',
  `user_db` varchar(50) DEFAULT '-',
  `password_db` varchar(32) DEFAULT '-',
  `nama_db` varchar(50) DEFAULT '-',
  `status_bank_apec` int(1) DEFAULT '0',
  `norek_aba` varchar(50) DEFAULT '-',
  `norek_abp` varchar(50) DEFAULT '-',
  `norek_abp_deposit` varchar(50) DEFAULT '-',
  PRIMARY KEY (`sandibank`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_intercon` */

/*Table structure for table `t_level_user` */

DROP TABLE IF EXISTS `t_level_user`;

CREATE TABLE `t_level_user` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `level` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `t_level_user` */

insert  into `t_level_user`(`id`,`level`) values (1,'Super User'),(2,'Supevisor Bank'),(3,'Admin Bank');

/*Table structure for table `t_producttab` */

DROP TABLE IF EXISTS `t_producttab`;

CREATE TABLE `t_producttab` (
  `Kodebng` varchar(3) NOT NULL,
  `namaproduk` varchar(20) DEFAULT NULL,
  `Sisbng` varchar(2) DEFAULT NULL,
  `BATAS1` double NOT NULL DEFAULT '0',
  `PRO1` double DEFAULT '0',
  `BATAS2` double DEFAULT '0',
  `PRO2` double DEFAULT '0',
  `BATAS3` double DEFAULT '0',
  `PRO3` double DEFAULT '0',
  `PRO4` double DEFAULT '0',
  `Pajak` double DEFAULT '0',
  `PPH` double DEFAULT '0',
  `REKBNG` varchar(9) DEFAULT NULL,
  `REKPJK` varchar(9) DEFAULT NULL,
  `KODE` varchar(1) DEFAULT NULL,
  `TIPEBNG` varchar(12) DEFAULT NULL,
  `REKACR` varchar(9) DEFAULT NULL,
  `jml_transaksi` double DEFAULT NULL,
  `nominal_transaksi` double DEFAULT NULL,
  PRIMARY KEY (`Kodebng`),
  KEY `namaproduk` (`namaproduk`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `t_producttab` */

insert  into `t_producttab`(`Kodebng`,`namaproduk`,`Sisbng`,`BATAS1`,`PRO1`,`BATAS2`,`PRO2`,`BATAS3`,`PRO3`,`PRO4`,`Pajak`,`PPH`,`REKBNG`,`REKPJK`,`KODE`,`TIPEBNG`,`REKACR`,`jml_transaksi`,`nominal_transaksi`) values ('101','TABUNGAN UMUM','1',0,0,0,2,0,0,0,0,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('104','TAB. PEND. KARYAWAN','1',0,0,0,8.75,0,0,0,0,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('601','TABUNGAN BPR','1',0,0,0,8,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('102','TABUNGAN PENDIDIKAN','1',0,0,0,8.75,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('103','TAB UMUM KARYAWAN','1',0,0,0,2,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('105','TABUNGAN BERHADIAH','1',0,0,0,8,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('108','TAB','1',0,0,0,3,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('106','TAB 2',NULL,0,0,0,5,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('107','TAB 3',NULL,0,0,0,4,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `level` int(1) DEFAULT NULL,
  `img_user` text,
  `sandibank` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
