var tabunganBersama = angular.module('tabunganBersama', []);

tabunganBersama.controller('userLevelCtrl', ['$scope', '$http',
  function userLevelCtrl($scope, $http) {
    $http.get('rest/user_level.php').success(function(data) {
      $scope.daftarUser = data;
    });
    $scope.orderUser = "username";
  }]);