<?php
	
	require_once('DB_Function.php');
	$db = new DB_Function();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <script type="text/javascript" src="jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="jquery.slidertron-1.1.js"></script>
        <script src="jquery.js"></script>
        <script src="MyMotion.js"></script>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800|Open+Sans+Condensed:300,700" rel="stylesheet" />
        <link href="default.css" rel="stylesheet" type="text/css" media="all" />
        <link href="fonts.css" rel="stylesheet" type="text/css" media="all" />
		<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->        
        <script type="text/javascript">
			$('#slider').slidertron({
				viewerSelector: '.viewer',
				reelSelector: '.viewer .reel',
				slidesSelector: '.viewer .reel .slide',
				advanceDelay: 3000,
				speed: 'slow',
				navPreviousSelector: '.previous-button',
				navNextSelector: '.next-button',
				indicatorSelector: '.indicator ul li',
				slideLinkSelector: '.link'
			});
		</script> 
    
	</head>
    
	<body>
		<div id="logo" class="container">
            <h1><span class="icon icon-lock icon-size"></span><a href="#">ANS <span>BANK INFORMATION SYSTEM</span></a></h1>
        </div>
		<!--<div id="banner">
	<div id="slider">
		<div class="viewer">
			<div class="reel">
				<div class="slide">
					<h2>This is the first slide.</h2>
					<p>Lorem ipsum dolor sit amet nullam.</p>
					<a class="link" href="http://nodethirtythree.com/#slidertron-slide-1">Full story ...</a> <img src="images/IMG1.jpg" alt="" /> </div>
				<div class="slide">
					<h2>This is the second slide.</h2>
					<p>Lorem ipsum dolor sit amet nullam.</p>
					<a class="link" href="http://nodethirtythree.com/#slidertron-slide-2">Full story ...</a> <img src="images/IMG2.jpg" alt="" /> </div>
				<div class="slide">
					<h2>This is the third slide.</h2>
					<p>Lorem ipsum dolor sit amet nullam.</p>
					<a class="link" href="http://nodethirtythree.com/#slidertron-slide-3">Full story ...</a> <img src="images/pic03.jpg" alt="" /> </div>
			</div>
		</div>
		<div class="indicator">
			<ul>
				<li class="active">1</li>
				<li>2</li>
				<li>3</li>
			</ul>
		</div>
	</div>-->
    
		<div id="wrapper" class="container">
			<div id="menu" class="container">
                <ul>
                    <li class="current_page_item"><a href="index.php" accesskey="1" title="">Homepage</a></li>
                    <li><a href="interkoneksi.php" accesskey="1" title="">Interkoneksi</a></li>
                    <li><a href="user.php" accesskey="2" title="">User</a></li>
                    <li><a href="#" accesskey="3" title="">Tentang Kami</a></li>
                    <li><a href="#" accesskey="4" title="">Hubungi Kami</a></li>
                    <!-- <li><a href="#" accesskey="5" title="">Contact Us</a></li> -->
                </ul>
			</div>
            
			<div id="three-column" class="container">
				<div>
                	<span class="arrow-down"></span>
                </div>
				<div id="tbox1">
                	<span class="icon icon-hdd"></span>
					<div class="title">
						<h2>Interkoneksi</h2>
					</div>
					<p></p>
					<a href="interkoneksi.php" class="button">Masuk</a> 
             	</div>
                
				<div id="tbox2"> 
                	<span class="icon icon-save"></span>
					<div class="title">
						<h2>User</h2>
					</div>
					<p></p>
					<a href="user.php" class="button">Masuk</a>
         		</div>
                        
				<div id="tbox3"> <span class="icon icon-sitemap"></span>
					<div class="title">
						<h2>Tentang Kami</h2>
					</div>
					<p></p>
					<a href="#" class="button">Masuk</a> 
              	</div>
			</div>
            
			<div id="page">
				<div>
                	<span class="arrow-down"></span>
                </div>
				<!--<div id="box1">
			<div class="title">
				<h2>Selamat datang di website kami</h2>
				<span class="byline">Integer sit amet pede vel arcu aliquet pretium</span> </div>
			<p>This is <strong>Justifiable</strong>, a free, fully standards-compliant CSS template designed by <a href="http://www.freecsstemplates.org/" rel="nofollow">FreeCSSTemplates.org</a>. The photos in this template are from <a href="http://fotogrph.com/"> Fotogrph</a>. This free template is released under a <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attributions 3.0</a> license, so you are pretty much free to do whatever you want with it (even use it commercially) provided you keep the links in the footer intact. Aside from that, have fun with it :) </p>
			<a href="#" class="button"></a>
		</div>-->
        
				<div id="box1">
					<div class="title">
                        <h2>Sekilas Tentang ANS</h2>
                        <span class="byline">Artha Nusa Sembada</span> 
                    </div>
                    <p>Lembaga Keuangan Mikro (Micro Finance Institution) sebagai ujung tombak dalam pengentasan kemiskinan dan dalam mendorong ekonomi masyarakat telah mendapat pengakuan secara internasional. Peranan Lembaga Keuangan Mikro (Micro Finance Institution) dalam memberikan akses permodalan kepada pelaku Usaha Mikro dan Menengah merupakan salah satu jalan keluar dalam pengentasan kemiskinan.
                                    <br></br>
                                   </p>	
                    <a href="#" class="button">Masuk</a>
				</div>
			</div>
            
			<!--<div id="portfolio">
		<div><span class="arrow-down"></span></div>
		<div class="title">
			<h2>Our Latest Work</h2>
			<span class="byline">Integer sit amet pede vel arcu aliquet pretium</span> </div>
		<ul>
			<li class="current_item"><a href="#" class="image image-full"><img src="images/pic01.jpg" alt="" /></a></li>
			<li><a href="#" class="image image-full"><img src="images/pic02.jpg" alt="" /></a></li>
			<li><a href="#" class="image image-full"><img src="images/pic03.jpg" alt="" /></a></li>
			<li><a href="#" class="image image-full"><img src="images/pic04.jpg" alt="" /></a></li>
			<li><a href="#" class="image image-full"><img src="images/pic05.jpg" alt="" /></a></li>
			<li><a href="#" class="image image-full"><img src="images/pic06.jpg" alt="" /></a></li>
		</ul>
		<div class="content">
			<p>Aliquam sem leo, vulputate sed, convallis at, ultricies quis, justo. Donec nonummy magna quis risus. Quisque eleifend. Phasellus tempor vehicula justo. Aliquam lacinia metus ut elit. Suspendisse iaculis mauris nec lorem. Vestibulum suscipit volutpat nulla.</p>
			<div class="actions"> <a href="#" class="button button-big">Get Started</a> <a href="#" class="button button-big button-alt">Learn More</a> </div>
		</div>
	</div>-->
    
            <div id="copyright">
                <p>Copyright (c) 2013 webAppANS.com. All rights reserved. | Photos by <a href="http://www.group-ans.com/">ANS</a> | Design by <a href="" rel="nofollow">illustr</a>.</p>
		</div>
	</body>
</html>
