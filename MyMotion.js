// JavaScript Document
function hideMe(element,duration){
    $(document).ready(function(){
        $(element).hide(duration);
    });
}

function hideTabelUser(element,duration){
    $(document).ready(function(){
        $(element).hide(duration);
        $("#form_input").show(duration);
    });
}

function showMe(element,duration){
    $(document).ready(function(){
        $(".rowHide").hide();
        $(element).show(duration);
    });
}

function moveMe(element,direction,distance){
    $(document).ready(function(){
        if(direction=='v'){
            $(element).animate({
                left:distance
            });
        } else if(direction=='h'){
            $(element).animate({
                top:distance
            });
        } else{
    }
    });
}

function btn_batal(){    
    $(document).ready(function(){
        $("#tabel_user").show('fast');
        $("#form_input").hide();
    });
}

function toggleMe(element,duration){
    $(document).ready(function(){
        $(element).toggle(duration);
    });
}

function saveMe(element1, element2, element3, element4, element5, element6, element7, element8, element9, element10){	
    $(document).ready(function(){
		
        if($(element1).val()==""||$(element2).val()==""||$(element3).val()==""||$(element4).val()==""||$(element5).val()==""||$(element6).val()==""||$(element7).val()==""||$(element8).val()==""||$(element9).val()==""){
            alert("Isi form yang masih kosong");
        }else{		
            $(this).click(function(){
                $.post("process.php",
                {
                    username : $(element1).val(),	
                    password  : $(element2).val(),
                    nama  : $(element3).val(),
                    jabatan : $(element4).val(),
                    email : $(element5).val(),
                    status : $(element6).val(),	
                    level : $(element7).val(),
                    img_user : $(element8).val(),	
                    sandibank : $(element9).val(),
                    operation : element10
                },
                function(data,status){
                    alert("Data: " + data + "\nStatus: " + status);	
			  
                    document.location = 'user.php';
                    $(".rowHide").hide();		
                });
            });       
        }
    }); 
}

function saveMeIntercon(element1, element2, element3, element4, element5, element6, element7, element8, element9, element10, element11, element12, element13){	
    $(document).ready(function(){
		
        if($(element1).val()==""||$(element2).val()==""||$(element3).val()==""||$(element4).val()==""||$(element5).val()==""||$(element6).val()==""||$(element7).val()==""||$(element8).val()==""||$(element9).val()==""||$(element10).val()=="" ||$(element11).val()=="" ||$(element12).val()=="" ||$(element13).val()==""){
            alert("Isi form yang masih kosong");
        }else{		
            $(this).click(function(){
                $.post("process.php",
                {
                    sandibank : $(element1).val(),	
                    namaBank  : $(element2).val(),
                    alamatBank : $(element3).val(),
                    ip  : $(element4).val(),
                    driver : $(element5).val(),
                    user_db  : $(element6).val(),
                    db_password : $(element7).val(),
                    port : $(element8).val(),
                    dbName : $(element9).val(),	
                    status_bankApec : $(element10).val(),
                    norek_aba : $(element11).val(),	
                    norek_abp_deposit : $(element12).val(),
                    operation : element13
                },
                function(data,status){
                    alert("Data: " + data + "\nStatus: " + status);	
			  
                    document.location = 'interkoneksi.php';
                    $(".rowHide").hide();		
                });
            });       
        }
    });
}

function deleteMe(element){
    $(document).ready(function(){
        var username = $(element).val();
        //alert(username);
        $.post("process.php",
        {
            username:username,
            operation:"delete"			  
        },
        function(data,status){
            alert("Data: " + data + "\nStatus: " + status);
            document.location = "user.php"
        });
    });
}

function deleteMeIntercon(element){
    $(document).ready(function(){
        var sandibank = $(element).val();
        //alert(username);
        $.post("process.php",
        {
            sandibank:sandibank,
            operation:"deleteIntercon"			  
        },
        function(data,status){
            alert("Data: " + data + "\nStatus: " + status);
            document.location = "interkoneksi.php"
        });
    });
}