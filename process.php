<?php

require_once("DB_Function.php");
require_once("Fungsi.php");

$db = new DB_Function();
$fungsi = new Fungsi();


// untuk di process.php
$operation = $_POST['operation'];
if ($operation == "update" or $operation == "create") {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $passwordEnc = $fungsi->EncryptText($password, "ADMIN123");
    $passwordEnc = $fungsi->EncryptText($passwordEnc, "ADMIN123");
    $nama = $_POST['nama'];
    $jabatan = $_POST['jabatan'];
    $email = $_POST['email'];
    $status = $_POST['status'];
    $level = $_POST['level'];
    $img_user = $_POST['img_user'];
    $sandibank = $_POST['sandibank'];
} else if ($operation == "delete") {
    $username = $_POST['username'];
}



if ($operation == "updateIntercon" or $operation == "createIntercon") {
    $sandibank = $_POST["sandibank"];
    $namaBank = $_POST["namaBank"];
    $alamatBank = $_POST["alamatBank"];
    $ip = $_POST["ip"];
    $driver = $_POST["driver"];
    $user_db = $_POST["user_db"];
    $db_password = $_POST["db_password"];

    $db_passwordEnc = $fungsi->EncryptText($db_password, "ADMIN123");
    $db_passwordEnc = $fungsi->EncryptText($db_passwordEnc, "ADMIN123");

    $port = $_POST["port"];
    $dbName = $_POST["dbName"];
    $status_bankApec = $_POST["status_bankApec"];
    $norek_aba = $_POST["norek_aba"];
    $norek_abp_deposit = $_POST["norek_abp_deposit"];
} else if ($operation == "deleteIntercon") {
    $sandibank = $_POST['sandibank'];
}

if ($operation == "update") {
    $db->updateT_user($username, $passwordEnc, $nama, $jabatan, $email, $status, $level, $img_user, $sandibank);
} else if ($operation == "create") {
    $db->insertT_user($username, $passwordEnc, $nama, $jabatan, $email, $status, $level, $img_user, $sandibank);
} else if ($operation == "delete") {
    $db->deleteT_user($username);
}

if ($operation == "updateIntercon") {
    $db->updateT_intercon($sandibank, $namaBank, $driver, $user_db, $port, $dbName, $status_bankApec, $norek_aba, $norek_abp_deposit, $alamatBank, $ip, $db_passwordEnc);
} else if ($operation == "createIntercon") {
    $db->insertT_intercon($sandibank, $namaBank, $driver, $user_db, $port, $dbName, $status_bankApec, $norek_aba, $norek_abp_deposit, $alamatBank, $ip, $db_passwordEnc);
} else if ($operation == "deleteIntercon") {
    $db->deleteT_intercon($sandibank);
}
?>