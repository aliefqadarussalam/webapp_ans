<!DOCTYPE html>
<html lang="en" ng-app="tabunganBersama">
  <head>
    <title>Test Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet" media="screen">
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    
  </head>
  <body ng-controller="userLevelCtrl">
    <h1>Simple page</h1>

  	<div class="container-fluid">
  		<div class="col-md-3">
  			<div class="span2 well">
  				<div class="side-navigation">
					<ul class="nav nav-list">
						<li>Search: <input type="text" ng-model="query"></li><br />
						<li><a href="#">Tambah User</a></li>
					</ul>
  				</div>
  			</div>
  		</div>
  		<div class="col-md-9">
  			<div class="span10">
  				
  				<ul>
  					<li ng-repeat="user in daftarUser | filter:query | orderBy:orderProp">
    				{{user.username}}
    				<p>{{user.nama}}</p>
  					</li>
  				</ul>
  			</div>
  		</div>
  	</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="public/js/jquery-1.9.0.js"></script>
    <script src="public/js/bootstrap.min.js"></script>
    <script src="public/js/angular.min.js"></script>
    <script src="controller/controller.js"></script>
  </body>
</html>