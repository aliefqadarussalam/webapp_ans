<?php
	
	class DB_Connect {
		
		// constructor
		function __construct(){
		}
		
		// destructor
		function __destruct(){
		}
		
		// connecting database
		public function connect(){
			require_once('config.php');
			
			// connection to mysql
			$con = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD);
			
			// selecting db
			mysql_select_db(DB_DATABASE);
			
			return($con);
		}
		
		public function close(){
			mysql_close();
		}
	}
?>